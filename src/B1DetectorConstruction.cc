#include "B1DetectorConstruction.hh"

#include "G4Material.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4UserLimits.hh"
#include "EBLResistSD.hh"

//___________________________________________________________________


B1DetectorConstruction::B1DetectorConstruction() : 
   G4VUserDetectorConstruction(), fScoringVolume(0)
{ }
//___________________________________________________________________

B1DetectorConstruction::~B1DetectorConstruction()
{ }
//___________________________________________________________________

G4VPhysicalVolume* B1DetectorConstruction::Construct()
{  
   // Get nist material manager
   G4NistManager* nist = G4NistManager::Instance();
   //G4Material* env_mat = nist->FindOrBuildMaterial("G4_WATER");

   // ------------------------------------------------------------------------
   // basic parameters
   double world_x = 3.0*cm;
   double world_y = 3.0*cm;
   double world_z = 3.0*cm;
   double working_distance    = 10*mm;
   double substrate_thickness = 1.0*mm; // Si (1mm) 
   double SiO2_thickness      = 1.0*um; // SiO2 (1um)
   double wire_thickness      = 10.0*nm; // Nb coating 
   double Cr_thickness        = 10.0*nm; // Cr coating 
   double resist_thickness    = 120.0*nm;
   double chuck_thickness     = 0.25*2.54*cm;
   double wafer_radius        = 0.5*cm;

   bool    checkOverlaps    = false;
   int     natoms           = 0;
   int     ncomponents      = 0;
   double  A                = 0.0;
   double  Z                = 0.0;
   double  thickness        = 0.0;
   double  surface_z        = 0.0;
   double  red              = 0.0;
   double  green            = 0.0;
   double  blue             = 0.0;
   double  alpha            = 0.0;
   double  density          = 0.0;
   double  pressure         = 0.0;
   double  temperature      = 0.0;

   // ------------------------------------------------------------------------
   // Elements
   // ------------------------------------------------------------------------
   G4Element* elH  = new G4Element("Hydrogen","H",  Z=1.,  A=1.00794*g/mole);
   G4Element* elSi = new G4Element("Silicon", "Si", Z=14., A= 28.0855*g/mole);
   G4Element* elO  = new G4Element("Oxygen",  "O",  Z=8.,  A= 15.9994*g/mole);
   G4Element* elC  = new G4Element("Carbon",  "C",  Z=6.,  A= 12.011 *g/mole);
   //G4Element* elN  = new G4Element("Nitrogen","N",  Z=7.,  A= 14.00674*g/mole);
   //G4Element* elNa = new G4Element("Sodium",  "Na", Z=11., A= 22.989768*g/mole);
   //G4Element* elAr = new G4Element("Argon",   "Ar", Z=18., A= 39.948*g/mole);
   //G4Element* elI  = new G4Element("Iodine",  "I",  Z=53., A= 126.90447*g/mole);
   //G4Element* elCs = new G4Element("Cesium",  "Cs", Z=55., A= 132.90543*g/mole);

   // ------------------------------------------------------------------------
   // World
   // ------------------------------------------------------------------------
   density     = universe_mean_density;
   pressure    = 1.e-7*bar;
   temperature = 0.1*kelvin;
   red         = 0.0/256.0;
   green       = 200.0/256.0;
   blue        = 0.0/256.0;
   alpha       = 0.4;

   G4Material        * world_mat   = new G4Material("world_mat", /*z=*/1.0, /*a=*/1.01*g/mole, density, kStateGas,temperature,pressure);
   G4Box             * world_solid = new G4Box( "World", 0.5*world_x, 0.5 * world_y, 0.5 * world_z );
   G4LogicalVolume   * world_log   = new G4LogicalVolume( world_solid, world_mat, "world_log" );
   G4VPhysicalVolume * world_phys  = new G4PVPlacement( 0, G4ThreeVector(), world_log, "world_phys", 0, false, 0, checkOverlaps );

   G4Colour            world_color {red, green, blue, alpha }; 
   G4VisAttributes   * world_vis   = new G4VisAttributes(world_color);
   //(*world_vis) = G4VisAttributes::GetInvisible();
   world_vis->SetForceWireframe(true);
   world_log->SetVisAttributes(world_vis);

   // ------------------------------------------------------------------------
   // Gun tip 
   // ------------------------------------------------------------------------
   G4double shape1_rmina  = 0.15*cm;
   G4double shape1_rmaxa  = 0.16*cm;
   G4double shape1_rminb  = 0.1*cm; 
   G4double shape1_rmaxb  = 0.11*cm;
   G4double shape1_hz     = 0.1*cm;
   G4double shape1_phimin = 0.*deg;
   G4double shape1_phimax = 360.*deg;

   red       = 0.0/256.0;
   green     = 0.0/256.0;
   blue      = 192.0/256.0;
   alpha     = 0.4;

   G4ThreeVector guntip_pos { 0, 0, -shape1_hz - working_distance };

   G4Material        * guntip_mat   = nist->FindOrBuildMaterial("G4_Al");
   G4Cons            * guntip_solid = new G4Cons("guntip_solid", shape1_rmina, shape1_rmaxa, shape1_rminb, shape1_rmaxb, shape1_hz, shape1_phimin, shape1_phimax);
   G4LogicalVolume   * guntip_log   = new G4LogicalVolume(guntip_solid, guntip_mat,"guntip_log");
   G4VPhysicalVolume * guntip_phys  = new G4PVPlacement(0,guntip_pos, guntip_log, "guntip_phys",world_log,false,0,checkOverlaps);                                  

   G4Colour            guntip_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * guntip_vis   = new G4VisAttributes(guntip_color);

   // ------------------------------------------------------------------------
   // Target
   // ------------------------------------------------------------------------
   // We are now going to add all the layers to the target volume wich matches
   // the total sum of material thicknesses. 
   double total_target_thickness = 0.0;
   total_target_thickness       += substrate_thickness;
   total_target_thickness       += SiO2_thickness;
   total_target_thickness       += wire_thickness;
   total_target_thickness       += Cr_thickness;
   total_target_thickness       += resist_thickness;
   total_target_thickness       += chuck_thickness;
   double target_offset          = total_target_thickness/2.0;

   // ------------------------------------------------------------------------
   // Target material volume
   // ------------------------------------------------------------------------
   // Includes chuck, substrate, resist, etc, 
   // The very top of the volume should be flush with the last material and is
   // the focal point used to define the working distance.

   double target_x      =  2.54*cm;
   double target_y      =  2.54*cm;
   double target_z      =  total_target_thickness;
   red                  =  250.0/256.0;
   green                =  0.0/256.0;
   blue                 =  0.0/256.0;
   alpha                =  0.4;

   G4ThreeVector target_pos  = { 0, 0, target_z/2.0 };

   G4Box             * target_solid = new G4Box( "target_solid", 0.5*target_x, 0.5*target_y, 0.5*target_z );
   G4LogicalVolume   * target_log   = new G4LogicalVolume( target_solid, world_mat, "target_log" );
   G4VPhysicalVolume * target_phys  = new G4PVPlacement( 0, target_pos, target_log, "target_phys", world_log, false, 0, checkOverlaps );

   G4Colour            target_color {red, green, blue, alpha }; 
   G4VisAttributes   * target_vis   = new G4VisAttributes(target_color);
   (*target_vis) = G4VisAttributes::GetInvisible();
   //target_vis->SetForceWireframe(true);
   target_log->SetVisAttributes(target_vis);

   // ---------------------------------------------------------
   // Al chuck : 1/4 in thick 
   // ---------------------------------------------------------
   thickness = chuck_thickness;
   density   = 2.330*g/cm3;
   red       = 120.0/256.0;
   green     = 120.0/256.0;
   blue      = 120.0/256.0;
   alpha     = 0.4;

   G4ThreeVector chuck_pos = { 0, 0, target_offset - thickness/2.0 };
   target_offset -= thickness;

   G4Material        * chuck_mat   = nist->FindOrBuildMaterial("G4_Al");
   G4VSolid          * chuck_solid  = new G4Tubs("chuck_solid", 0.0, wafer_radius, thickness/2.0, 0.0, 360.*deg );
   G4LogicalVolume   * chuck_log    = new G4LogicalVolume(chuck_solid, chuck_mat, "chuck");      
   G4VPhysicalVolume * chuck_phys   = new G4PVPlacement(0, chuck_pos, chuck_log, "chuck_phys", target_log, false, 0, checkOverlaps);

   G4Colour      chuck_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * chuck_vis    = new G4VisAttributes(chuck_color);
   chuck_log->SetVisAttributes( chuck_vis );

   // ---------------------------------------------------------
   // Silicon Substrate : 1 mm thick, monocrystalline 
   // ---------------------------------------------------------
   thickness = substrate_thickness;
   red       = 192.0/256.0;
   green     = 192.0/256.0;
   blue      = 192.0/256.0;
   alpha     = 0.4;

   G4ThreeVector substrate_pos = { 0, 0, target_offset - thickness/2.0 };
   target_offset -= thickness;

   G4Material        * substrate_mat   = nist->FindOrBuildMaterial("G4_Si");
   G4VSolid          * substrate_solid  = new G4Tubs("substrate_solid", 0.0, wafer_radius, thickness/2.0, 0.0, 360.*deg );
   G4LogicalVolume   * substrate_log    = new G4LogicalVolume(substrate_solid, substrate_mat, "Substrate");      
   G4VPhysicalVolume * substrate_phys   = new G4PVPlacement(0, substrate_pos, substrate_log, "substrate_phys", target_log, false, 0, checkOverlaps);

   G4Colour      substrate_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * substrate_vis    = new G4VisAttributes(substrate_color);
   substrate_log->SetVisAttributes( substrate_vis );

   // ---------------------------------------------------------
   // Si02 - Substrate coating
   // ---------------------------------------------------------
   thickness = SiO2_thickness;
   red       = 192.0/256.0;
   green     = 192.0/256.0;
   blue      = 1.0/256.0;
   alpha     = 0.4;

   G4ThreeVector Si02_pos { 0, 0, target_offset - thickness/2.0 };
   target_offset -= thickness;

   G4Material        * SiO2_mat    = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
   G4VSolid          * SiO2_solid  = new G4Tubs("SiO2_solid", 0.0, wafer_radius, thickness/2.0, 0.0, 360.0         * deg );
   G4LogicalVolume   * SiO2_log    = new G4LogicalVolume(SiO2_solid, SiO2_mat, "SiO2_log");
   G4VPhysicalVolume * SiO2_phys   = new G4PVPlacement(0, Si02_pos, SiO2_log, "SiO2_phys", target_log, false, 0, false);

   G4Colour            SiO2_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * SiO2_vis    = new G4VisAttributes(SiO2_color);
   SiO2_log->SetVisAttributes( SiO2_vis );

   G4UserLimits * SiO2_limits = new G4UserLimits(0.01*um);
   SiO2_log->SetUserLimits(SiO2_limits);

   // ---------------------------------------------------------
   // Nb wire coating 
   // ---------------------------------------------------------
   thickness = wire_thickness;
   density   = 2.330*g/cm3;
   red       = 127.0/256.0;
   green     = 255.0/256.0;
   blue      = 212.0/256.0;
   alpha     = 0.4;

   G4ThreeVector wire_pos = { 0, 0, target_offset - thickness/2.0 };
   target_offset -= thickness;

   G4Material        * wire_mat   = nist->FindOrBuildMaterial("G4_Nb");
   G4VSolid          * wire_solid  = new G4Tubs("wire_solid", 0.0, wafer_radius, thickness/2.0, 0.0, 360.*deg );
   G4LogicalVolume   * wire_log    = new G4LogicalVolume(wire_solid, wire_mat, "wire");      
   G4VPhysicalVolume * wire_phys   = new G4PVPlacement(0, wire_pos, wire_log, "wire_phys", target_log, false, 0, checkOverlaps);

   G4Colour      wire_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * wire_vis    = new G4VisAttributes(wire_color);
   wire_log->SetVisAttributes( wire_vis );

   G4UserLimits * wire_limits = new G4UserLimits(0.002*um);
   wire_log->SetUserLimits(wire_limits);

   // ---------------------------------------------------------
   // Cr coating 
   // ---------------------------------------------------------
   thickness = wire_thickness;
   density   = 2.330*g/cm3;
   red       = 178/256.0;
   green     = 34.0/256.0;
   blue      = 34/256.0;
   alpha     = 0.4;

   G4ThreeVector Cr_pos = { 0, 0, target_offset - thickness/2.0 };
   target_offset -= thickness;

   G4Material        * Cr_mat   = nist->FindOrBuildMaterial("G4_Cr");
   G4VSolid          * Cr_solid  = new G4Tubs("Cr_solid", 0.0, wafer_radius, thickness/2.0, 0.0, 360.*deg );
   G4LogicalVolume   * Cr_log    = new G4LogicalVolume(Cr_solid, Cr_mat, "Cr");      
   G4VPhysicalVolume * Cr_phys   = new G4PVPlacement(0, Cr_pos, Cr_log, "Cr_phys", target_log, false, 0, checkOverlaps);

   G4Colour      Cr_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * Cr_vis    = new G4VisAttributes(Cr_color);
   Cr_log->SetVisAttributes( Cr_vis );

   G4UserLimits * Cr_limits = new G4UserLimits(0.002*um);
   Cr_log->SetUserLimits(Cr_limits);

   // ---------------------------------------------------------
   // FOx-22 Resist
   // ---------------------------------------------------------
   // HSQ - Hydrogen silsesquioxane
   // Most studied type is the cubic cluster H_{8}Si_{8}O_{12} 
   // [https://en.wikipedia.org/wiki/Hydrogen_silsesquioxane]
   //
   // Dow's FOx-22 has the following composition according to the MSDS: 
   // CAS Number    Spec.Grav.   Wt%          Formula                   Component Name
   //   107-51-7    0.82         55-75  C_{8}H_{24}O_{2}Si{3O})   Octamethyltrisiloxane (OMTS)
   //   107-46-0    0.764        15-35  C_{6}H_{18}OSi_{2}        Hexamethyldisiloxane (HMDS)
   //   137125-44-1 0.88         10-30  H_{8}Si_{8}O_{12}         Hydrogen Silsesquioxane, Hydroxy-terminated (HSQ)
   //   108-88-3    0.87         <1         C_{7}H_{8}                Toluene
   // 
   // FOx-22 has SG (at 25C) : 0.81
   // therefore the density is roughly = SG_water(25C/4C) SG_HSQ rho_water
   // rho_HSQ = 0.998363*0.999972*0.81 [g/cm^3] = 0.808651
   //
   // We will construct the resist compound with weight percentages of 59 : 20.25 : 20.25 : 0.5

   G4Material * OMTS_mat  = new G4Material("Octamethyltrisiloxane-OMTS", density=0.82*g/cm3, /*n_comps=*/4);
   OMTS_mat->AddElement( elC,  /*number_of_atoms=*/ 8);
   OMTS_mat->AddElement( elH,  /*number_of_atoms=*/ 24);
   OMTS_mat->AddElement( elO,  /*number_of_atoms=*/ 2);
   OMTS_mat->AddElement( elSi, /*number_of_atoms=*/ 30);

   G4Material * HMDS_mat  = new G4Material("Hexamethyldisiloxane-HMDS", density=0.764*g/cm3, /*n_comps=*/4);
   HMDS_mat->AddElement( elC,  /*number_of_atoms=*/ 6);
   HMDS_mat->AddElement( elH,  /*number_of_atoms=*/ 18);
   HMDS_mat->AddElement( elO,  /*number_of_atoms=*/ 1);
   HMDS_mat->AddElement( elSi, /*number_of_atoms=*/ 12);

   G4Material * HSQ_mat  = new G4Material("Hexamethyldisiloxane-HSQ", density=0.88*g/cm3, /*n_comps=*/3);
   HSQ_mat->AddElement( elH,  /*number_of_atoms=*/ 8);
   HSQ_mat->AddElement( elSi, /*number_of_atoms=*/ 8);
   HSQ_mat->AddElement( elO,  /*number_of_atoms=*/ 12);

   G4Material * Toluene_mat  = nist->FindOrBuildMaterial("G4_TOLUENE");

   G4Material * FOx22_mat = new G4Material("FOx_22", density=0.81*g/cm3, /*n_comps=*/4);
   FOx22_mat->AddMaterial( OMTS_mat,     /*fractionMass=*/ 59.00*perCent );
   FOx22_mat->AddMaterial( HMDS_mat,     /*fractionMass=*/ 20.25*perCent );
   FOx22_mat->AddMaterial( HSQ_mat,      /*fractionMass=*/ 20.25*perCent );
   FOx22_mat->AddMaterial( Toluene_mat,  /*fractionMass=*/  0.50*perCent );

   // ---------------------------------------------------------

   thickness = resist_thickness;
   red       = 0.0/256.0;
   green     = 255.0/256.0;
   blue      = 127.0/256.0;
   alpha     = 0.4;

   G4ThreeVector resist_pos { 0, 0, target_offset - thickness/2.0 };
   target_offset -= thickness;

   G4VSolid          * resist_solid = new G4Tubs("resist_solid", 0.0, wafer_radius, thickness/2.0, 0.0, 360.0*deg );
   G4LogicalVolume   * resist_log   = new G4LogicalVolume(resist_solid, FOx22_mat, "resist_log");
   G4VPhysicalVolume * resist_phys  = new G4PVPlacement(0, resist_pos, resist_log, "resist_phys", target_log, false,0,false);

   G4Colour            resist_color {red, green, blue, alpha };   // Gray 
   G4VisAttributes   * resist_vis    = new G4VisAttributes(resist_color);
   resist_log->SetVisAttributes( resist_vis );

   G4UserLimits * resist_limits = new G4UserLimits(0.004*um);
   resist_log->SetUserLimits(resist_limits);

   EBLResistSD * resist_det = new EBLResistSD("/resistSD");
   SetSensitiveDetector("resist_log",resist_det);


   // --------------------------------------------------------

   fScoringVolume = resist_log;

   return world_phys;
}
//___________________________________________________________________


