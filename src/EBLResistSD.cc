#include "EBLResistSD.hh"
#include "EBLResistHit.hh"
#include "G4Step.hh"
#include "G4Track.hh"
#include "G4HCofThisEvent.hh"
#include "G4TouchableHistory.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "B1Run.hh"

EBLResistSD::EBLResistSD(G4String name) : G4VSensitiveDetector(name)
{
   G4String HCname;
   collectionName.insert(HCname="hitsCollection");

   HCID = -1;

   fAnalysisManager = G4AnalysisManager::Instance();

   // Creating histograms
   fhBackward_0 = fAnalysisManager->CreateH1("back0","Backward scattered energies", 100,0,100);
   fhForward_0  = fAnalysisManager->CreateH1("forw0","Forward scattered energies",  100,0,100);
   fhBackScatXYEnergyWt      = fAnalysisManager->CreateH2("fhBackScatXYEnergyWt","Backward XY energy wts ",         100,-20,20,200,-20,20);
   fhForwardScatXYEnergyWt   = fAnalysisManager->CreateH2("fhForwardScatXYEnergyWt","Forwardward XY energy wts ",   100,-20,20,200,-20,20);

   fhBackScatXYEnergyWt_1    = fAnalysisManager->CreateH2("fhBackScatXYEnergyWt_1","Backward XY energy wts ",       200,-3,3,200,-3,3);
   fhForwardScatXYEnergyWt_1 = fAnalysisManager->CreateH2("fhForwardScatXYEnergyWt_1","Forward scattering zoomed ", 200,-0.01,0.01,200,-0.01,0.01);

   fhBackScat_XY       = fAnalysisManager->CreateH2("fhBackScat_XY",    "Backward XY",     200,-20,20, 200,-20,20);
   fhForwardScat_XY    = fAnalysisManager->CreateH2("fhForwardScat_XY", "Forwardward XY",  200, -1, 1, 200, -1, 1);
   fhBackScat_XY_1     = fAnalysisManager->CreateH2("fhBackScat_XY_1",    "Backward XY",     200,-1,1, 200,-1,1);
   fhForwardScat_XY_1  = fAnalysisManager->CreateH2("fhForwardScat_XY_1", "Forwardward XY",  200, -0.01, 0.01, 200, -0.01, 0.01);

   fhEDD_x_y0  = fAnalysisManager->CreateH2("fhEDD_x_y0", "EDD along y0 ",  200, -60, 60, 200, 0.0,120.0);
   fhEDD_y_x0  = fAnalysisManager->CreateH2("fhEDD_y_x0", "EDD along x0 ",  200, -60, 60, 200, 0.0,120.0);
   fhEDD_x_y0_1  = fAnalysisManager->CreateH2("fhEDD_x_y0_1", "EDD along y0 ",  200, -5, 5, 200, 0.0,120.0);
   fhEDD_y_x0_1  = fAnalysisManager->CreateH2("fhEDD_y_x0_1", "EDD along x0 ",  200, -5, 5, 200, 0.0,120.0);
   fhEDD_x_y0_2  = fAnalysisManager->CreateH2("fhEDD_x_y0_2", "EDD along y0 ",  200, -5, 5, 100, 0.0,120.0);
   fhEDD_y_x0_2  = fAnalysisManager->CreateH2("fhEDD_y_x0_2", "EDD along x0 ",  200, -5, 5, 100, 0.0,120.0);
   fhEDD_x_y0_3  = fAnalysisManager->CreateH2("fhEDD_x_y0_3", "EDD along y0 ",  200, -2, 2, 200, 0.0,120.0);
   fhEDD_y_x0_3  = fAnalysisManager->CreateH2("fhEDD_y_x0_3", "EDD along x0 ",  200, -2, 2, 200, 0.0,120.0);

   fhEDD_x_y0_4  = fAnalysisManager->CreateH2("fhEDD_x_y0_4", "EDD along y0 ",  100, -2, 2, 20, 0.0,120.0);
   fhEDD_y_x0_4  = fAnalysisManager->CreateH2("fhEDD_y_x0_4", "EDD along x0 ",  100, -2, 2, 20, 0.0,120.0);

   fhEDDNonIonizing_x_y0  = fAnalysisManager->CreateH2("fhEDDNI_x_y0", "Non Ionizing EDD along y0 ",  200, -5, 5, 200,0.0, 120.0);
   fhEDDNonIonizing_y_x0  = fAnalysisManager->CreateH2("fhEDDNI_y_x0", "Non Ionizing EDD along x0 ",  200, -5, 5, 200,0.0, 120.0);

   if(fhEDD_x_y0 < 0 ) std::cout << "bad fhEDD_x_y0" << fhEDD_x_y0 << std::endl;
   if(fhEDD_y_x0 < 0 ) std::cout << "bad fhEDD_y_x0" << fhEDD_y_x0 << std::endl;
   if(fhEDDNonIonizing_x_y0 < 0 ) std::cout << "bad fhEDDNonIonizing_x_y0" << fhEDDNonIonizing_x_y0 << std::endl;
   if(fhEDDNonIonizing_y_x0 < 0 ) {
      std::cout << "bad fhEDDNonIonizing_y_x0" << fhEDDNonIonizing_y_x0 << std::endl;
      exit(-1);
   }
}
//______________________________________________________________________________

EBLResistSD::~EBLResistSD()
{ }
//______________________________________________________________________________

void EBLResistSD::Initialize(G4HCofThisEvent* HCE)
{
   hitsCollection = new EBLResistHitsCollection(SensitiveDetectorName,collectionName[0]); 

   if(HCID<0) { HCID = GetCollectionID(0); }
   HCE->AddHitsCollection(HCID,hitsCollection);
}
//______________________________________________________________________________

G4bool EBLResistSD::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
   // Fill histograms

   G4StepPoint* preStep = aStep->GetPreStepPoint();
   G4TouchableHistory* touchable = (G4TouchableHistory*)(preStep->GetTouchable());

   double pz           = aStep->GetPreStepPoint()->GetMomentum().z();
   double energy       = aStep->GetPreStepPoint()->GetKineticEnergy()/keV;
   G4ThreeVector pos = aStep->GetPreStepPoint()->GetPosition();
   double dE_step     = aStep->GetTotalEnergyDeposit()/keV;
   double dE_non_ion  = aStep->GetNonIonizingEnergyDeposit()/keV;
   if( aStep->IsLastStepInVolume() ) {
      if( pz < 0.0 ) {
         fAnalysisManager->FillH1( fhBackward_0, energy);
         fAnalysisManager->FillH2( fhBackScatXYEnergyWt, pos.x()/um, pos.y()/um, energy);
         fAnalysisManager->FillH2( fhBackScatXYEnergyWt_1, pos.x()/um, pos.y()/um, energy);
         fAnalysisManager->FillH2( fhBackScat_XY, pos.x()/um, pos.y()/um);
         fAnalysisManager->FillH2( fhBackScat_XY_1, pos.x()/um, pos.y()/um);

         ////std::cout << energy << "\n";
      } else {
         fAnalysisManager->FillH1(fhForward_0, energy);
         //fRun->fhForwardScatEnergy->Fill( energy  );
         fAnalysisManager->FillH2( fhForwardScatXYEnergyWt, pos.x()/um, pos.y()/um, energy);
         fAnalysisManager->FillH2( fhForwardScatXYEnergyWt_1, pos.x()/um, pos.y()/um, energy);
         fAnalysisManager->FillH2( fhForwardScat_XY, pos.x()/um, pos.y()/um);
         fAnalysisManager->FillH2( fhForwardScat_XY_1, pos.x()/um, pos.y()/um);
      }
   }
   if(dE_step > 0.0) {
      //std::cout << " dE_step    = " << dE_step    << std::endl;;
      //std::cout << " dE_non_ion = " << dE_non_ion << std::endl;;
      //std::cout << pos.x()/nm << " " << pos.z()/nm << std::endl;
      fAnalysisManager->FillH2( fhEDD_x_y0 , pos.x()/nm, pos.z()/nm, dE_step);
      fAnalysisManager->FillH2( fhEDD_y_x0 , pos.y()/nm, pos.z()/nm, dE_step);

      fAnalysisManager->FillH2( fhEDD_x_y0_1 , pos.x()/nm, pos.z()/nm, dE_step);
      fAnalysisManager->FillH2( fhEDD_y_x0_1 , pos.y()/nm, pos.z()/nm, dE_step);

      fAnalysisManager->FillH2( fhEDD_x_y0_2 , pos.x()/nm, pos.z()/nm, dE_step);
      fAnalysisManager->FillH2( fhEDD_y_x0_2 , pos.y()/nm, pos.z()/nm, dE_step);

      fAnalysisManager->FillH2( fhEDD_x_y0_3 , pos.x()/nm, pos.z()/nm, dE_step);
      fAnalysisManager->FillH2( fhEDD_y_x0_3 , pos.y()/nm, pos.z()/nm, dE_step);

      fAnalysisManager->FillH2( fhEDD_x_y0_4 , pos.x()/nm, pos.z()/nm, dE_step);
      fAnalysisManager->FillH2( fhEDD_y_x0_4 , pos.y()/nm, pos.z()/nm, dE_step);

      //fAnalysisManager->FillH2( fhEDDNonIonizing_x_y0 , pos.x()/nm, pos.z()/nm, dE_non_ion);
      //fAnalysisManager->FillH2( fhEDDNonIonizing_y_x0 , pos.y()/nm, pos.z()/nm, dE_non_ion);
   }
   // Ensure counting incoming tracks only.
   //if ( preStep->GetStepStatus() == fGeomBoundary ){
   //   EBLResistHit* newHit = new EBLResistHit();
   //   newHit->SetStripNo(  touchable->GetReplicaNumber(0) );
   //   newHit->SetPosition( aStep->GetPreStepPoint()->GetPosition() );
   //   newHit->SetMomentum( aStep->GetPreStepPoint()->GetMomentum() );
   //   newHit->SetEnergy( aStep->GetPreStepPoint()->GetTotalEnergy() );
   //   newHit->SetParticle( aStep->GetTrack()->GetDefinition() );
   //   hitsCollection->insert( newHit );
   //}
   return true;
}
//______________________________________________________________________________

void EBLResistSD::EndOfEvent(G4HCofThisEvent*)
{ }
//______________________________________________________________________________

void EBLResistSD::clear()
{ } 
//______________________________________________________________________________

void EBLResistSD::DrawAll()
{ } 
//______________________________________________________________________________

void EBLResistSD::PrintAll()
{ } 
//______________________________________________________________________________

