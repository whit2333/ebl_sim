#ifndef EBLResistSD_h
#define EBLResistSD_h 1

#include "G4VSensitiveDetector.hh"
#include "EBLResistHit.hh"
#include "B1Analysis.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class EBLResistSD : public G4VSensitiveDetector
{
  public:
     G4AnalysisManager * fAnalysisManager;

     G4int  fhForward_0 ;
     G4int  fhBackward_0;
     G4int  fhBackScatEnergy;
     G4int  fhForwardScatEnergy;
     G4int  fhBackScatXYEnergyWt;
     G4int  fhForwardScatXYEnergyWt;
     G4int  fhBackScatXYEnergyWt_1;
     G4int  fhForwardScatXYEnergyWt_1;

     G4int  fhBackScat_XY;
     G4int  fhForwardScat_XY;
     G4int  fhBackScat_XY_1;
     G4int  fhForwardScat_XY_1;

     G4int fhEDD_x_y0;
     G4int fhEDD_y_x0;
     G4int fhEDD_x_y0_1;
     G4int fhEDD_y_x0_1;

     G4int fhEDD_x_y0_2;
     G4int fhEDD_y_x0_2;

     G4int fhEDD_x_y0_3;
     G4int fhEDD_y_x0_3;

     G4int fhEDD_x_y0_4;
     G4int fhEDD_y_x0_4;

     G4int fhEDDNonIonizing_x_y0;
     G4int fhEDDNonIonizing_y_x0;

  public:
      EBLResistSD(G4String name);
      ~EBLResistSD();

      void Initialize(G4HCofThisEvent*HCE);
      G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
      void EndOfEvent(G4HCofThisEvent*HCE);
      void clear();
      void DrawAll();
      void PrintAll();

  private:
      G4int HCID;
      EBLResistHitsCollection *hitsCollection;

};




#endif

